const map = {
    //steemit
    elipowell: 'Steemit',
    steemitblog: 'Steemit',
    steemitdev: 'Steemit',
    //hive
    hiveio: 'hive',

    // Add Custom Badges. Use single quotes for the key if user has . or -,  e.g.
    // 'robot.pay' : 'Robot',
};

export function getAffiliation(token, user) {
    if (map[token] && map[token][user]) {
        return map[token][user];
    } else if (map[user]) {
        return map[user];
    } else {
        return '';
    }
}

export function affiliationFromStake(token, accountName, stake) {
    // Put stake based breakdowns here.
    if (accountName === 'proofofbrainio') {
        return 'Tribe Leader';
    }
    ///////////////// Selling only

    /////////////// Flair for Stakes
    if (stake >= 25000) {
        return 'Hero Tribe Member';
    } else if (stake >= 15000) {
        return 'Legendary Tribe Member';
    } else if (stake >= 10000) {
        return 'Full Tribe Member';
    } else if (stake >= 2500) {
        return 'Tribe Member';
    } else if (stake >= 500) {
        return 'minnow Tribe Member';
    } else if (stake >= 50) {
        return 'Litlefish Tribe Member';
    } else {
        return null;
    }
}

export default map;
